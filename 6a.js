// Swap the values of variables A and B

// terdapat 2 variabel A & B
let A = 3;
let B = 5;

// Tukar Nilai variabel A dan B, Syarat Tidak boleh menambah Variabel Baru

A = A + B;
B = A - B;
A = A - B;

console.log(`After swapping: A = ${A}, B = ${B}`);

// Hasil yang diharapkan :
// A = 5
// B = 3
