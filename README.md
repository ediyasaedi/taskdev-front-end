# Frontend Developer

## Questions

1. Do you prefer vuejs or reactjs? Why ?
2. What complex things have you done in frontend development ?
3. why does a UI Developer need to know and understand UX? how far do you understand it?
4. Give your analysis results regarding https://taskdev.mile.app/login from the UI / UX side!
5. Create a better login page based on https://taskdev.mile.app/login and deploy on https://www.netlify.com (https://www.netlify.com/)!
6. Solve the logic problems below !

## Answers

1. I prefer to use reactjs, as it can strengthen my knowledge of javascript with a very large community complexity. because basically the react concept and its supporting libraries are not much different from the javascript concept itself. Learning React allows me not only to build websites but also mobile apps with React Native.
2. The abstraction we use to manipulate the data into the structure we want is constantly changing, making it complicated and difficult.
3. Because the application we make is for use by other users / people, we as developers must provide a good experience so that our application can continue to be used. The essence of UX as far as I understand is how to create a simple yet powerful interface.
4. A. Since there is not much information displayed on the page it is better without scrolling. <br/>
   B. because only 2 languages ​​are available, it is better if the user is only one click away to change the language. <br/>
   C. footer can be displayed better for branding. <br/>
   D. back to MileApp is better replaced with the MileApp logo which if clicked leads to the main MileApp page.
5. Link deploy on https://5faba651e9224a57614f9c7c--taskdevlogin-edi.netlify.app/
6. Find all answers in other files in this directory (6a.js, 6b.js, 6c.js, 6d.js).
